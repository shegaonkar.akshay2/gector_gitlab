from flask import Flask, render_template, request
import re
from grammar_checker import Checker

app = Flask(__name__)
checker = Checker()

@app.route('/', methods=['POST', 'GET'])
def homepage():
    if request.method == 'POST':
        input = request.form['input']
        
        output = checker.check_grammar(input)
        content = {
            'input': input,
            'output': output,
            'count': count_words(output)
        }
        return render_template("home.html", grammar=content)
    else:
        return render_template("home.html")


def count_words(string):
    l = string.split(' ')
    return len(l)

if __name__ == "__main__":
    app.run(debug=True)
